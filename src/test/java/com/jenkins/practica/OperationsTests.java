package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations dogops = new DogsOperations();
		String MyDog = dogops.getRandomDogImage();
		// Assert true if the result string ends with '.jpg'
		assertEquals(".jpg", MyDog.substring(MyDog.length() - 4));
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 * @param <dogops>
	 */
	
	@Test
	public <dogops> void breedsList() {
		// Instantiate DogsOperations class
		DogsOperations dogops = new DogsOperations();
		// Call getBreedList operation and store the result on ArrayList
		 ArrayList<String> MyDogList = dogops.getBreedList();
		// Assert true if the result ArrayList has size of more than 0
		assertTrue(MyDogList.size()>0);
	}
}
